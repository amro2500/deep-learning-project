package com.deeplearningproject.covid_classifier;

/*
 * Deep Learning Project - 2021/2022 SEM1
 * Group Members:
 * - Amr Abdeltawab - MKE201052
 * - Monzer Raslan - MKE191184
 * - Ahmed Alazzani
 *
 * This package intends to classify images of X-Ray images and label them as Covid-19 Infected, Normal, Infected with
 * Viral Pneumonia.
 *
 * Dataset Origin: https://www.kaggle.com/pranavraikokte/covid19-image-dataset
 */


import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.BaseImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.deeplearning4j.zoo.*;
import org.deeplearning4j.zoo.model.*;
import org.nd4j.common.io.ClassPathResource;
import org.nd4j.evaluation.classification.Evaluation;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;

import java.io.File;
import java.util.Random;

public class CovidClassifier {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CovidClassifier.class);

    private static final String [] allowedExtensions = BaseImageLoader.ALLOWED_FORMATS;
    private static final long seed = 1000;
    private static final Random randNumGen = new Random(seed);

    private static final int height = 224;
    private static final int width = 224;
    private static final int channels = 3;
    private static final int batchSize = 10;
    private static final int numOutput = 3;
    private static final int labelIndex = 1;
    private static final int nEpochs = 10;
    private static final double learningRate = 0.001;

    private static final DataNormalization scale = new ImagePreProcessingScaler(0, 1);

    public static void main(String[] args) throws Exception {
        // define image folder location
        File trainDir = new ClassPathResource("Covid19-dataset/train").getFile();
        File testDir = new ClassPathResource("Covid19-dataset/test").getFile();

        //Files in directories under the parent dir that have "allowed extensions" split needs a random number generator for reproducibility when splitting the files into train and test
        FileSplit train = new FileSplit(trainDir, allowedExtensions, randNumGen);
        FileSplit test = new FileSplit(testDir, allowedExtensions, randNumGen);

        //You do not have to manually specify labels. This class (instantiated as below) will
        //parse the parent dir and use the name of the subdirectories as label/class names
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();

        //Specifying a new record reader with the height and width you want the images to be resized to.
        //Note that the images in this example are all of different size
        //They will all be resized to the height and width specified below
        ImageRecordReader trainRR = new ImageRecordReader(height,width,channels,labelMaker);
        ImageRecordReader testRR = new ImageRecordReader(height,width,channels,labelMaker);

        //Initialize the record reader with the train data and the transform chain
        trainRR.initialize(train);
        testRR.initialize(test);

        int numLabels = trainRR.numLabels();


        DataSetIterator trainIterator = new RecordReaderDataSetIterator(trainRR, batchSize, labelIndex, numLabels);
        DataSetIterator testIterator = new RecordReaderDataSetIterator(testRR, batchSize, labelIndex, numLabels);

        // Scaling values from 0 to 1;
        scale.fit(trainIterator);
        trainIterator.setPreProcessor(scale);
        scale.fit(testIterator);
        testIterator.setPreProcessor(scale);

        // Build Our Neural Network
        log.info("**** Build Model ****");

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .updater(new Nesterovs(learningRate, Nesterovs.DEFAULT_NESTEROV_MOMENTUM))
                .weightInit(WeightInit.XAVIER)
                .list()
                .layer(0, new ConvolutionLayer.Builder(5, 5)
                        .nIn(channels)
                        .stride(1, 1)
                        .nOut(32)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                .layer(2, new ConvolutionLayer.Builder(5, 5)
                        .nIn(channels)
                        .stride(1, 1)
                        .nOut(64)
                        .activation(Activation.IDENTITY)
                        .build())
                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                .layer(4, new DenseLayer.Builder().activation(Activation.RELU)
                        .nOut(128).build())
                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(numOutput)
                        .activation(Activation.SOFTMAX)
                        .build())
                .setInputType(InputType.convolutionalFlat(height, width, 3)) // InputType.convolutional for normal image
                .backpropType(BackpropType.Standard)
                .build();

//        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
//                .seed(seed)
////                .updater(new Adam(learningRate))
//                .updater(new Nesterovs(learningRate, Nesterovs.DEFAULT_NESTEROV_MOMENTUM))
//                .weightInit(WeightInit.XAVIER)
//                .list()
//                .layer(0, new ConvolutionLayer.Builder(3, 3)
//                        .nIn(channels)
//                        .stride(1, 1)
//                        .nOut(32)
//                        .activation(Activation.IDENTITY)
//                        .build())
//                .layer(1, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(2, new ConvolutionLayer.Builder(3, 3)
//                        .nIn(channels)
//                        .stride(1, 1)
//                        .nOut(64)
//                        .activation(Activation.IDENTITY)
//                        .build())
//                .layer(3, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(4, new ConvolutionLayer.Builder(3, 3)
//                        .nIn(channels)
//                        .stride(1, 1)
//                        .nOut(128)
//                        .activation(Activation.IDENTITY)
//                        .build())
//                .layer(5, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(6, new ConvolutionLayer.Builder(3, 3)
//                        .nIn(channels)
//                        .stride(1, 1)
//                        .nOut(256)
//                        .activation(Activation.IDENTITY)
//                        .build())
//                .layer(7, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
//                        .kernelSize(2, 2)
//                        .stride(2, 2)
//                        .build())
//                .layer(8, new DenseLayer.Builder().activation(Activation.RELU)
//                        .nOut(128).build())
//                .layer(9, new DenseLayer.Builder().activation(Activation.RELU)
//                        .nOut(64).build())
//                .layer(10, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
//                        .nOut(numOutput)
//                        .activation(Activation.SOFTMAX)
//                        .build())
//                .setInputType(InputType.convolutionalFlat(height, width, 3)) // InputType.convolutional for normal image
//                .backpropType(BackpropType.Standard)
//                .build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        model.setListeners(new ScoreIterationListener(10));

        // Save Our Neural Network Model
//        log.info("**** Save Model ****");
//        File locationToSave = new File("trained_covid19_model.zip");
        // ModelSerializer needs modelName, Location, saveUpdater
//        ModelSerializer.writeModel(model,locationToSave, false);

        // evaluation while training (the score should go down)
        for (int i = 0; i < nEpochs; i++) {
            model.fit(trainIterator);

            log.info("Completed epoch {}", i);
            Evaluation eval = model.evaluate(testIterator);
            log.info(eval.stats());
            trainIterator.reset();
            testIterator.reset();
        }
    }
}
