# Deep Learning Project

Deep Learning Project - 2021/2022 SEM1 

Group Members:
* Amr Abdeltawab - MKE201052
* Monzer Raslan - MKE191184
* Ahmed Alazzani - MEE211005

This package intends to classify images of X-Ray images and label them as Covid-19 Infected, Normal, Infected with Viral
Pneumonia using CNN (VGG16)

Dataset Origin: https://www.kaggle.com/pranavraikokte/covid19-image-dataset
